## ReactJS, Hooks, create-react-app

luxoft 

### Case Study TopMenu

**Task:**

Please create React component &lt;Top Menu /&gt;.
For this purpose, please use available endpoint
(http://www.mocky.io/v2/5d3fec2b33000062009d27bc) to fetch menu items. Menu should be positioned on the top of the page (default menu position should be fixed, but it should be changable by
props to any css position property like absolute, relative, static) Menu items should be toggable (should
reveal subitems on mouse click). User can easily change the styling of the component by passing new
classNames into the component

#### What needs to be done to complete the task
1. Fetch data from the available API url.
2. If one of the menu items is selected (toggle on) sub menu will appear and we highlight currently
selected item (only the menu item should be highlighted)
3. If one of the menu items is selected and we will click on another menu item, we should hide
previously selected and highlight the current one (open submenu)
4. Only one submenu can be opened at the time
5. Use default position prop set to fixed.
6. Application should be covered by unit tests (testing framework free of choice)

### Spent time

2 hours
 
### Run project 

```sh
yarn
npm start
```
### Run tests

```sh
npm run test
```
### Developer
[@nickkireev]

[@nickkireev]: <https://bitbucket.org/nickkireev>
