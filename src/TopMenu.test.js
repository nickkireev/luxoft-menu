import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {render, fireEvent, cleanup} from '@testing-library/react';
import TopMenu from './TopMenu.js';

configure({ adapter: new Adapter() });

const data = [
  {
    "id": 0,
    "name": "Item0",
    "subItems": [
      {
        "id": 0,
        "name": "SubMenu0"
      },
      {
        "id": 1,
        "name": "SubMenu1"
      },
      {
        "id": 2,
        "name": "SubMenu2"
      }
    ]
  },
];

describe('TopMenu', () => {
  it('TopMenu renders without crashing with data = []', () => {
    const menu = shallow(<TopMenu data={[]} />);

    expect(menu).toEqual({})
  });

  it('TopMenu render SubMenu after click', () => {
    const {getByText} = render(<TopMenu data={data} />);

    getByText(/Item0/i)
    expect(getByText(/Item0/i)).not.toBeUndefined();

    fireEvent.click(getByText(/Item0/i));
    expect(getByText(/SubMenu0/i)).not.toBeUndefined();
    expect(getByText(/SubMenu1/i)).not.toBeUndefined();
    expect(getByText(/SubMenu2/i)).not.toBeUndefined();
  });
});



afterEach(cleanup);
