import React, { useState } from 'react';
import PropTypes from 'prop-types';
import MenuItem from './MenuItem.js';

function TopMenu({ data, position }) {
    const [click, setClick] = useState(null);
    if (!data || data.length === 0) {
        return null;
    }
    const pos = ['absolute', 'fixed', 'relative', 'static', 'inherit'].find((item) => item === props.position);
    return (
        <div style={{ position: pos || 'fixed' }}>
            <ul>
                {data.map(item => (<MenuItem key={item.id} data={item} clicked={click === item.id} onClick={setClick}/>))}
            </ul>
        </div>
    );
}

TopMenu.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        subItems: PropTypes.array,
    })).isRequired,
    position: PropTypes.string,
};

export default TopMenu;
