import React, {
    useState,
    useEffect,
} from 'react';
import axios from 'axios';
import TopMenu from './TopMenu.js';

function App() {
    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios('http://www.mocky.io/v2/5d3fec2b33000062009d27bc');
            setData(result.data);
        };

        fetchData();
    }, []);

    return (<TopMenu data={data} position="fixed"/>);
}

export default App;
