import React from 'react';
import PropTypes from 'prop-types';

function MenuItem({ data, clicked, onClick, className, style }) {
    if (!data || data.length === 0) {
        return null;
    }
    return (
        <div>
            <li
                className={className}
                style={{ textDecoration: clicked ? 'underline' : 'none', ...style }}
                onClick={() => { onClick(data.id); }}
            >
                {data.name}
            </li>
            {clicked && data.subItems && <ul>
                {data.subItems.map((item) => (<li key={item.id}>{item.name}</li>))}
            </ul>}
    </div>
    );
}

MenuItem.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
    }),
};

export default MenuItem;
